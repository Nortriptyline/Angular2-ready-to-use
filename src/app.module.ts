import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';

//Pages
import { RootPage } from './pages/root.page';


// Components
import { AppComponent } from './components/app.component';

@NgModule ({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    AppComponent,
    RootPage
  ],
  providers: [
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
