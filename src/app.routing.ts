import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RootPage } from './pages/root.page';
const appRoutes: Routes = [
  {
    path: "",
    component: RootPage,
    pathMatch: "full"
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
