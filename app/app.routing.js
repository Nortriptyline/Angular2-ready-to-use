"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var root_page_1 = require("./pages/root.page");
var appRoutes = [
    {
        path: "",
        component: root_page_1.RootPage,
        pathMatch: "full"
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map